A web-based HTML5 player for your music catalog.

In the past I served my MP3 collection from a Raspberry Pi using the DAAP prototocol via the node-daap project. iTunes speaks this protocol so it worked well for a while... This project is to replace DAAP and iTunes, since really, a browser is all we need these days.

Browsing the catalog:

![Catalog browser](https://www.greaterscope.net/images/music-player5.png "Catalog browser")

Playlist:

![Playlist](https://www.greaterscope.net/images/music-player6.png "Playlist")

Installation
===

1. npm install
2. cd into public folder
3. Run node catalog.js /PATH/TO/MUSIC
4. This generates catalog.json with your music
5. Create a symlink at music-player/public/music pointing to your music folder from step 3
6. Point nginx or another webserver to the music-player/public folder
7. Be sure to enable gzip for application/json and text/html content types if you have a large catalog
