var walk = require('walk'),
    fs = require('fs'),
    mm = require('musicmetadata'),
    arguments = process.argv.splice(2),
    directory = arguments[0];

if (!directory) {
    console.log('usage: node server.js FOLDER');
    return;
}

var catalogFile = 'catalog.json';
var songs = [];
var previousSongsCache = {};
var currentSongsCache = {};
var verbose = true;

var artistCorrections = {
    "blink-182": "Blink 182",
    'Adam & The Ants': 'Adam Ant',
    'Appleseed Cast': 'The Appleseed Cast',
    'Daft Punk feat. Julian Casablancas': 'Daft Punk',
    'Daft Punk feat. Panda Bear': 'Daft Punk',
    'Daft Punk feat. Paul Williams': 'Daft Punk',
    'Daft Punk feat. Pharrell Williams': 'Daft Punk',
    'Daft Punk feat. Todd Edwards': 'Daft Punk',
    'Hopesfall': 'HopesFall',
    'Minus The Bear': 'Minus the Bear',
    'Paul Gardiner/Gary Numan': 'Gary Numan',
    'Prefab Sprout ': 'Prefab Sprout',
    'Pretenders': 'The Pretenders',
    'of Montreal': 'Of Montreal'
};

var addFolder = function(folder, callback) {
    var options = {
        followLinks: false,
    };
    var walker = walk.walk(folder, options);

    if (verbose) {
        console.log('Walking ' + folder);
    }
    walker.on("file", function (root, fileStats, next) {
        var filepath = root + '/' + fileStats.name;
        var filename = filepath.replace(folder, '');
        var format;
        // do we need to check that fileStats.type == 'file'?
        // For now, we'll support jpg covert art, mp3 and flac files
        if (fileStats.name.match(/\.mp3$/i)) {
            format = 'mp3';
        } else if (fileStats.name.match(/\.ogg$/i)) {
            format = 'ogg';
        } else if (fileStats.name.match(/\.flac$/i)) {
            format = 'flac';
        } else {
            next();
            return;
        }

        if (filename in previousSongsCache) {
            var song = currentSongsCache[filename] = previousSongsCache[filename];
            if (verbose) {
                console.log('Skipping: ' + filename);
            }
            if (song.artist in artistCorrections) {
                song.artist = artistCorrections[song.artist];
            }
            if (song.albumArtist in artistCorrections) {
                song.albumArtist = artistCorrections[song.albumArtist];
            }

            // why was i using song year in the comparison ... why does that matter?
            song.compare = song.albumArtist.toLowerCase() + '/' + song.album.toLowerCase() + '/' + song.discNumber + '/' + ("0000" + song.trackNumber).slice(-4);
            songs.push( song );
            next();
            return;
        }

        if (verbose) {
            console.log('Adding new song: ' + filename);
        }
        var readStream = fs.createReadStream(filepath);
        var parser = mm(readStream, {duration:true}, function (err, tags) {
            if (err) {
                return callback(err);
            }

            var artist = tags.artist[0];
            if (artist in artistCorrections) {
                artist = artistCorrections[artist];
            }

            var albumArtist;
            if (albumArtist = tags.albumartist[0]) {
                if (albumArtist in artistCorrections) {
                    albumArtist = artistCorrections[albumArtist];
                }
            } else {
                albumArtist = artist;
            }

            var song = {
                // file path, relative to Music folder and webroot
                file: filename,
                size: fileStats.size,
                format: format,
                // from ID3 tags
                name: tags.title, // will replace this with mp3 tag title later
                artist: artist,
                artistId: 0, // Filled in later
                albumArtist: albumArtist,
                albumArtistId: 0,
                artist_album: artist + '/' + tags.album,
                albumArtist_album: albumArtist + '/' + tags.album,
                artists: tags.artist,
                album: tags.album,
                albumId: 0, // Filled in later
                year: tags.year || '0000',
                time: tags.duration,
                genre: tags.genre[0] || '',
                trackNumber: tags.track ? tags.track.no : 1,
                trackCount: tags.track ? tags.track.of : 1,
                discNumber: tags.disk ? tags.disk.no : 1,
                discCount: tags.disk ? tags.disk.of : 1,

                compare: ''
            };

            song.compare = song.albumArtist.toLowerCase() + '/' + song.album.toLowerCase() + '/' + song.discNumber + '/' + ("0000" + song.trackNumber).slice(-4);

            currentSongsCache[filename] = song;
            songs.push( song );

            readStream.destroy();

            next();
        });
    });

    walker.on("errors", function (root, nodeStatsArray, next) {
        console.log('errors', nodeStatsArray);
        next();
    });

    walker.on("end", callback);
};


var persistSongs = function() {
    /*
    {
        "artists": [
            // artist of index 0
            "Blink 182"
        ],
        "albums": [
            // album of index 0
            "Take off your pants and jacket"
        ],
        "songs": [
            // song of index 0
            {"name":"Some blink song", "filename":"song.mp3"}
        ],
        "albumsByArtist": [
            // artist index 0, has these album indexes
            [0]
        ],
        "songsByArtist": [
            // artist index 0, has these song indexes
            [0]
        ]
    }
    */
    var out = {
        artists: [],
        albumArtists: [],
        albums: [],
        songs: songs,
        songsByArtist: [],
        songsByAlbumArtist: [],
        songsByAlbum: []
    };


    songs.sort(function(a, b) {
        if (a.compare < b.compare) {
            return -1;
        } else if (a.compare > b.compare) {
            return 1;
        }
        return 0;
    });

    var artists = {};
    var artistCount = 0;
    var albumArtists = {};
    var albumArtistCount = 0;

    var albums = {};
    var albumCount = 0;
    for (var songId = 0; songId < out.songs.length; songId++) {
        var song = out.songs[songId];
        var artistId;
        var albumArtistId;
        var albumId;


        // Artist to Song map
        if (!(song.artist in artists)) {
            artistId = artistCount;
            artists[ song.artist ] = artistId;
            out.artists.push( song.artist );
            out.songsByArtist.push( [] );
            artistCount++;
        } else {
            artistId = artists[ song.artist ];
        }
        song.artistId = artistId;


        // Album Artist to Song map
        if (!(song.albumArtist in albumArtists)) {
            albumArtistId = albumArtistCount;
            albumArtists[ song.albumArtist ] = albumArtistId;
            out.albumArtists.push( song.albumArtist );
            out.songsByAlbumArtist.push( [] );
            albumArtistCount++;
        } else {
            albumArtistId = albumArtists[ song.albumArtist ];
        }
        song.albumArtistId = albumArtistId;


        // Album to Song map
        if (!(song.albumArtist_album in albums)) {
            albumId = albumCount;
            albums[ song.albumArtist_album ] = albumId;
            out.albums.push( song.album );
            out.songsByAlbum.push( [] );
            albumCount++;
            
        } else {
            albumId = albums[ song.albumArtist_album ];
        }
        song.albumId = albumId;

        out.songsByArtist[ artistId ].push(songId);
        out.songsByAlbumArtist[ albumArtistId ].push(songId);
        out.songsByAlbum[ albumId ].push(songId);
    }

    fs.writeFile(process.cwd() + '/' + catalogFile, JSON.stringify(out), function(err) {
        if (err) {
            console.log('Failed to persist music catalog to disk: ' + err);
            return;
        }
        console.log('Saved music catalog to disk');
    });

    fs.writeFile(process.cwd() + '/songs.json', JSON.stringify(currentSongsCache), function(err) {
        if (err) {
            console.log('Failed to persist songs cache to disk: ' + err);
            return;
        }
        console.log('Saved songs cache to disk');
    });
};

if (directory[0] != '/') {
    console.log('Please specify an absolute path. Relative paths are not supported yet');
} else {

    if (directory[-1] != '/') {
        directory += '/';
    }

    var songsCacheFile = process.cwd() + '/songs.json';
    if (fs.existsSync(songsCacheFile)) {
        var json = fs.readFileSync(songsCacheFile);
        previousSongsCache = JSON.parse(json);
    }
    addFolder(
        directory,
        function(error) {
            if (error) {
                console.log(error);
                return;
            }
            setTimeout(persistSongs, 1000);
        }
    );
}
